#include "PointCloud.h"

/*!
\param pointCloud an PointCloud class argument.
\return new appended PointCloud class
*/
PointCloud* PointCloud::appendPointCloud(PointCloud* pointCloud) {
	int newPointNumber = pointCloud->getPointNumber();
	for (int i = getPointNumber(); i < getPointNumber() + newPointNumber; i++)
	{
		points.push_back(pointCloud->points[i - pointCloud->pointNumber]);
	}
	setPointNumber(getPointNumber() + pointCloud->getPointNumber());

	return getPointCloud();
}

/*!
\param nothing argument.
\return PointCloud class
*/
PointCloud* PointCloud::getPointCloud() {
	PointCloud* result = new PointCloud(pointNumber);
	result->points = points;
	result->pointNumber = pointNumber;

	return result;
}

/*!
\param point an Point class argument.
\return PointCloud class
*/
void PointCloud::addToPointCloud(Point point) {
	points.push_back(point);
	pointNumber++;
}

