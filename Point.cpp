#include "Point.h"
#include <cmath>

/*!
\param point an reference Point class.
\return bool
*/
bool Point::operator==(Point& point) 
{
	if (x == point.x && y == point.y && z == point.y) {
		return 1;
	}
	else {
		return 0;
	}
}

/*!
\param toPoint an Point class argument.
\return distance double
*/
double Point::distance(Point* toPoint) {
	return sqrt(pow(toPoint->x - x, 2) + pow(toPoint->y - y, 2) + pow(toPoint->z - z, 2));
}

/*!
\param toPoint an Point class argument.
\return distance double
*/
double Point::distanceToOrigin(Point* toPoint) {
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}

